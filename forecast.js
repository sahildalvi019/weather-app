//http://api.openweathermap.org/data/2.5/forecast?q=Mumbai&units=metric&APPID=043f397ea4c467a5e5acdf1e9b1e7033

const key ="043f397ea4c467a5e5acdf1e9b1e7033";

const getForecast = async (city) => {
    const base = "http://api.openweathermap.org/data/2.5/forecast";
    const query = `?q=${city}&units=metric&APPID=${key}`;
    
    const response = await fetch(base+query);
    // console.log(response);
    if(response.ok){
        const data = await response.json();
        return data;
    }else{
        throw new Error('Error status ' + response.status);
    }
}

//getForecast()
//    .then(data => console.log(data))
//    .catch(err => console.error(err));